/**
 * Separate Blur Shader
 * 
 * This blur shader works by applying two successive passes, one horizontal
 * and the other vertical.
 * 
 * Press the mouse to switch between the custom and default shader.
 */
 
PShader blur;
PGraphics src;
PGraphics pass1, pass2;

Arcball arcball;

Extrusion extrusion;
 
void setup() 
{
  size(800, 600, P2D);
  
  arcball = new Arcball(width/2, height/2, 600);  
 
  blur = loadShader("blur.glsl");
  blur.set("blurSize", 30);
  blur.set("sigma", 10.0f);  
 
  src = createGraphics(width, height, P3D); 
 
  pass1 = createGraphics(width, height, P2D);
  pass1.noSmooth();  
 
  pass2 = createGraphics(width, height, P2D);
  pass2.noSmooth();
  
  PVector[] points = new PVector[400];
  
  for (int i = 0; i < points.length; i++)
  {
    float k = map(i, 0, points.length - 1, 0.0, 1.0);
    float y = map(k, 0, 1, -10, 10);
    float x = sin(k * PI * 2.0 * 4.0) * 10;
    float z = cos(k * PI * 2.0 * 4.0) * 10;
    points[i] = new PVector(x, y, z);
  }
  
  // float topRadius, float bottomRadius, PVector[] points, int sides
  extrusion = new Extrusion(4.0, 4.0, points, 32);
  
  frameRate(60);
}
 
void draw() 
{ 
  //blur.set("blurSize", (int)map(mouseX, 0, width, 0, 32));
  //blur.set("sigma", map(mouseY, 0, height, 0, 10)); 
 
  arcball.run(src);
  
  src.beginDraw();
  src.background(0);
  src.ambient(80);   
  //src.lights();
  src.fill(255);
  src.translate(width/2, height/2, 200);
  //src.ellipse(0, 0, 100, 100);
  extrusion.draw(src);
  src.endDraw();
 
  // Applying the blur shader along the vertical direction   
  blur.set("horizontalPass", 0);
  pass1.beginDraw();            
  pass1.shader(blur);  
  pass1.image(src, 0, 0);
  pass1.endDraw();
 
  // Applying the blur shader along the horizontal direction      
  blur.set("horizontalPass", 1);
  pass2.beginDraw();            
  pass2.shader(blur);  
  pass2.image(pass1, 0, 0);
  pass2.endDraw();    
 
  image(pass2, 0, 0);
 
  fill(255, 0, 0);
  text(frameRate, 20, 20);
}
 
void mousePressed(){
  arcball.mousePressed();
}

void mouseDragged(){
  arcball.mouseDragged();
}