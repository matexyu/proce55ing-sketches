size(1024, 768);
background(255);
smooth();

float lines_spacing = width / 13.0;
float lines_thickness = lines_spacing / 8.0;

// Draw gray lines
stroke(153);
strokeWeight(lines_thickness);

for (float x = lines_spacing / 2.0; x < width; x += lines_spacing)
{
  line(x, 0, x, height);
}

for (float y = lines_spacing / 2.0; y < height; y += lines_spacing)
{
  line(0, y, width, y);
}

for (float y = -lines_spacing - width; y < width; y += lines_spacing * 2)
{
  line(0, y, width, y + width);
  line(width, y, 0, y + width);
}

// Draw white points
stroke(255);
float radius = lines_thickness * 1.5;
strokeWeight(radius * 0.2);
fill(0);
ellipseMode(CENTER);

final float d = radius;
for (float x = lines_spacing / 2.0; x < width; x += lines_spacing * 4)
{
  for (float y = lines_spacing / 2.0; y < height; y += lines_spacing * 4)
  {
    ellipse(x, y, d, d);
  }
}