
int counter = 0;

void setup()
{
  size(1920, 1080, P3D);
  frameRate(60);
  smooth();
}

void draw()
{
  background(0);
  
  for(int i = 0; i < 1920; i += 10)
  {
    int xx = (counter + i) % 1920;
    int x = xx;
    int y = xx;
    float w = 1920;
    float h = 1080;
    stroke(255);
    if(y <= 1080) line(0, y, w, y);
    line(x, 0, x, h);
  }
  
  counter++;
}