/**
 * Example based on oscP5plug example by andreas schlegel
 * oscP5 website at http://www.sojamo.de/oscP5
 */

import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

// 60 Fotogramas Por Segundo (FPS)
float FPS = 60;


// Parametros del circulo
float cx, cy; // centro del circulo
float cr; // radio del circulo

// Parametros del movimento del centro del circulo
float angle; // direcciòn del movimento
float speed; // velocidad del movimento

void setup() 
{
  size(800, 600, OPENGL);
  frameRate(FPS);
  
  cx = width / 2;
  cy = height / 2;
  
  // start oscP5, listening for incoming messages at port 12000
  oscP5 = new OscP5(this, 12000);
  
  myRemoteLocation = new NetAddress("127.0.0.1", 12001);
 
  oscP5.plug(this, "receive_fiddle", "/fiddle");
}



public void receive_fiddle(float amplitude, float pitch) 
{
  println("receive_fiddle() 2 floats:"+
    " pitch = " + pitch + 
    ", amplitude = " + amplitude);
    
  // angolo en radiantes (0 ... 2 Pi griego)
  angle = map(pitch, 24, 60, 0, PI*2);
  // velocidad (speed) en pixeles por segundo
  speed = map(amplitude, 0, 1, 0, 600); // max speed 0 600 pixeles por segundo
}


void draw() 
{
  // Simula animación
  float dt = 1.0 / FPS; // delta tiempo (duración de un fotograma en segundos)
  float dir_x = cos(angle);   
  float dir_y = sin(angle);
  cx += dir_x * speed * dt;
  cy += dir_y * speed * dt;
  
  // el radio depende de la velocidad
  cr = 8 + speed/3; 
  
  // Wrapping (derecha-izquierda / arriba-abajo)
  cx = (cx + width) % width;
  cy = (cy + height) % height;
  
  // Pinta la escena:
  
  //background(0);
  //fill(0,0,0,20); rect(0,0,width,height);
  
  color c = color(speed + 80);
  fill(c);
  ellipse(cx, cy, cr, cr); 
  
  if(false) // pinta el vector de movimento
  {
    stroke(255,0,0);
    line(cx, cy, cx + dir_x * speed, cy + dir_y * speed);
    stroke(0);
  }
}

