/**
 * Circle Collision with Swapping Velocities
 * by Ira Greenberg. 
 * 
 * Based on Keith Peter's Solution in
 * Foundation Actionscript Animation: Making Things Move!
 */
 
ArrayList<Ball> balls = new ArrayList<Ball>();

void setup() {
  size(1024, 1024);

  int num_x = 32;
  int num_y = 32;
  float max_r = 15;
  
  for(int i = 0; i < num_x; i++)
  {
    float ox = map(i, 0, num_x - 1, max_r, width - max_r);
    
    for(int j = 0; j < num_y; j++)
    {
      float oy = map(j, 0, num_y - 1, max_r, height - max_r);
      float r = i==0&&j==0? 32 : 5;//random(max_r - 4) + 5;
      balls.add(new Ball(ox, oy, r));
    }
  }
  smooth(1);
  frameRate(60);
}

void draw() {
  background(51);

  int num_steps = 2;
  float step_ratio = 1.0 / (float)num_steps;
  for(int step = 0; step < num_steps; step++)
  {
    for (Ball b : balls)
    {
      b.update(step_ratio);
      b.checkBoundaryCollision();
    }
  }
  noStroke();
  
  int bb = 0;
  for (Ball b : balls)
  {
    if(bb == 0) fill(255, 64, 64, 200);
    else if(bb == 1) fill(200, 16);
    b.display();
    bb++;
  }
  
  for(int i = 0; i < balls.size(); i++)
  {
    for(int j = i + 1; j < balls.size(); j++)
    {
      balls.get(i).checkCollision(balls.get(j));
    }
  }
}