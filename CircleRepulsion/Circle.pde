class Circle {
  
  PVector center;
  float radius;
  
  Circle(float r)
  {
    center = new PVector();
    radius = r;
  }
  
  Circle(float x, float y, float r)
  {
    center = new PVector(x, y);
    radius = r;
  }
  
  Circle clone()
  {
    return new Circle(center.x, center.y, radius);
  }
  
  void setRadius(float r)
  {
    radius = r;
  }
  
  void setCenter(float x, float y)
  {
    center.x = x;
    center.y = y;
  }
  
  float distanceFrom(Circle other)
  {
    float radiuses_sum = other.radius + this.radius;
    return PVector.dist(center, other.center) - radiuses_sum;
  }
  
  void draw()
  {
    float d = radius * 2.0;
    ellipse(center.x, center.y, d, d);
  }
  
  void clipToRectangle(float left, float right, float top, float bottom)
  {
    float r = radius;
    if(center.x < left + r) center.x = left + r;
    if(center.x > right - r) center.x = right - r;
    if(center.y < top + r) center.y = top + r;
    if(center.y > bottom - r) center.y = bottom - r;
  }
  

}