
ArrayList<Circle> mCirclesSource;
ArrayList<Circle> mCirclesTarget;

ArrayList<Circle> mCirclesA = new ArrayList<Circle>();
ArrayList<Circle> mCirclesB = new ArrayList<Circle>();

int mCycle;
void swapCircleArrays()
{
  if(mCycle == 0)
  {
    mCirclesSource = mCirclesA;
    mCirclesTarget = mCirclesB;
  }
  else
  {
    mCirclesSource = mCirclesB;
    mCirclesTarget = mCirclesA;
  }
  
  mCycle = (mCycle + 1) % 2;
}

void setup() 
{
  size(1024, 1024);
  smooth(1);
  frameRate(60);
  
  swapCircleArrays();
  
  float radius = 64.0;
  
  mCirclesSource.add(new Circle(radius));
  
  for(int i = 0; i < 24; i++)
  {
    float x = random(width);
    float y = random(height);
    Circle circle = new Circle(x, y, radius);
    mCirclesSource.add(circle);
  }
  
  for(Circle c : mCirclesSource)
  {
    mCirclesTarget.add(new Circle(radius));
  }
}

void clipToScreenRectangle(Circle circle)
{
  circle.clipToRectangle(0.0, width-1.0, 0.0, height-1.0);
}

PVector makeVector(PVector from, PVector to)
{
  PVector to_copy = to.copy();
  return to_copy.sub(from);
}

void simulate(double delta_seconds)
{ 
  int di = (mCycle == 0 ? 1 : -1);
  int start_i = (mCycle == 0 ? 0 : mCirclesSource.size() - 1);
  
  for(int i = start_i, count_i = 0; count_i < mCirclesSource.size(); i += di, count_i++)
  {
    Circle circle_a = mCirclesSource.get(i);
    
    for(int j = 0; j < mCirclesSource.size(); j++)
    {
      if(i == j) continue;
      
      Circle circle_b = mCirclesSource.get(j);
      
      Circle circle_target_b = mCirclesTarget.get(j);
      
      float radiuses_sum = circle_a.radius + circle_b.radius;
      
      if(abs(circle_a.center.x - circle_b.center.x) > radiuses_sum) continue;
      if(abs(circle_a.center.y - circle_b.center.y) > radiuses_sum) continue;
      
      PVector distance_vector = makeVector(circle_a.center, circle_b.center);
      float distance_vector_mag = distance_vector.mag();
      float distance = distance_vector_mag - radiuses_sum - 0.5f;
      
      circle_target_b.center = circle_b.center.copy();
      
      if(distance < 0.0)
      {
        if(distance_vector_mag > 0.0)
        { 
          float speed_ratio = map(-distance, circle_a.radius, 0.0, 1.0, 0.01);
          speed_ratio = pow(speed_ratio, 1.2);
        
          distance_vector.normalize();
          distance_vector.mult(1.0 + (float)((speed_ratio * circle_a.radius * 10.0) * delta_seconds));          
          
          circle_target_b.center.add(distance_vector);
        }
        else
        {
          PVector randy = PVector.random2D().mult(2.0);
          circle_target_b.center.add(randy);
        }
        
      }
      
      clipToScreenRectangle(circle_target_b);
    }
  }
}

double mOldTimeMilliseconds;

void draw()
{
  double current_time_seconds = millis() / 1000.0;
  double delta_seconds = current_time_seconds - mOldTimeMilliseconds;
  mOldTimeMilliseconds = current_time_seconds;
  
  background(64);
  mCirclesSource.get(0).setCenter(mouseX, mouseY);
  mCirclesTarget.get(0).setCenter(mouseX, mouseY);
  
  for(int iteration = 0; iteration < 8; iteration++)
  {
    simulate(delta_seconds);
    swapCircleArrays();
    
    mCirclesSource.get(0).setCenter(mouseX, mouseY);
    mCirclesTarget.get(0).setCenter(mouseX, mouseY);
  }
  
  noStroke();
  fill(255, 128);
  
  int i = 0;
  for(Circle circle : mCirclesB)
  {
    if(i == 0) { fill(255, 64, 64, 128); i++; }
    else if(i == 1) { fill(255, 128); i++; }
    circle.draw();
  }
  
  
}