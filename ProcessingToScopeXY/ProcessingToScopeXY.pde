// import everything necessary to make sound.
import ddf.minim.*;
import ddf.minim.ugens.*;

// create all of the variables that will need to be accessed in
// more than one methods (setup(), draw(), stop()).
Minim minim;
AudioOutput out;
Constant freqControl;

ScopeXY mScope;

void setup()
{
  // initialize the drawing window
  size(640, 640, P3D);
  
  frameRate(60.0);
  
  // initialize the minim and out objects
  minim = new Minim(this);
  
  out = minim.getLineOut(Minim.STEREO, 2048);
  
  ScopeXY mScope = new ScopeXY();
  mScope.patch(out);
  
}

// draw is run many times
void draw()
{ 

}

ArrayList<?> points = new ArrayList<PVector>();

void drawScopeSimulation()
{
  // erase the window to black
  background(8, 32, 40);
  
    // draw using a white stroke
  stroke( 64, 255, 64, 200 );
  //strokeWeight(1.5);
  noFill();
  beginShape();
  
  final float margin_px = 20;
  for( int i = 0; i < out.bufferSize() - 1; i++ )
  {
    float x = map( out.left.get(i), -1, 1, 0 + margin_px, width - margin_px);
    float y = map( out.right.get(i), -1, 1, 0 + margin_px, height - margin_px);
    curveVertex(x, y);
  }
  
  endShape();
}