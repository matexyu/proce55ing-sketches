class Particle 
{
  public Particle()
  {
  }
  
  void set(double x, double y, double vx, double vy)
  {
    this.x = x;
    this.y = y;
    this.vx = vx;
    this.vy = vy;
  }
  
  void update(double dt)
  {
    //vy += -1.0*dt;
    
    if (x > 1.0)
    {
      x = 1.0;
      vx = -vx;
    }
    
    if (x < -1.0)
    {
      x = -1.0;
      vx = -vx;
    }
    
    if (y < -1.0)
    {
      y = -1.0;
      vy = -vy;
    }
    
    if (y > 1.0)
    {
      y = 1.0;
      vy = -vy;
    }
    
    x += vx*dt;
    y += vy*dt;
  }
  
  public double x, y;
  double vx, vy;
};