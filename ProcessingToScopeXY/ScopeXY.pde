import ddf.minim.*;
import ddf.minim.ugens.*;

public class ScopeXY extends UGen
{
  /**
   * UGens patched to this input should generate values between -1 and +1.
   * 
   * @example Synthesis/panExample
   * 
   * @related Pan
   * @related setPan ( )
   */
  public UGenInput    pan;

  private final int numChannels = 2;
  private UGen      audio;
  private float[]   tickBuffer = new float[numChannels];
  
  public ScopeXY()
  {
    super();
    pan = addControl(0.0);
    
    mParticle.set(0,0, 1, 1);
  }
  
  public void setPan(float panValue)
  {
    pan.setLastValue( panValue );
  }

  @Override
  protected void addInput(UGen in)
  {
    // System.out.println("Adding " + in.toString() + " to Pan.");
    audio = in;
    audio.setChannelCount( numChannels );
  }

  @Override
  protected void removeInput(UGen input)
  {
    if ( audio == input )
    {
      audio = null;
    }
  }

  @Override
  protected void sampleRateChanged()
  {
    if ( audio != null )
    {
      audio.setSampleRate( sampleRate() );
      mSampleRate = sampleRate();
      println("Sample Rate changed to: " + mSampleRate);
    }
  }

  /**
   * Pan overrides setChannelCount to ensure that it can 
   * never be set to output more or fewer than 2 channels.
   */
  @Override
  public void setChannelCount(int numberOfChannels)
  {
    if ( numberOfChannels == 2 )
    {
      super.setChannelCount( numberOfChannels );
    }
    else
    {
      throw new IllegalArgumentException( "Pan MUST be ticked with STEREO output! It doesn't make sense in any other context!" );
    }
  }

  /**
   * NOTE: Currently only supports stereo audio!
   */
  @Override
  protected void uGenerate(float[] channels)
  {
    if ( channels.length != 2 )
    {
      throw new IllegalArgumentException( "Pan MUST be ticked with STEREO output! It doesn't make sense in any other context!" );
    }

    if ( audio != null )
    {
      audio.tick( tickBuffer );
    }
    
    /*
    frames_counter++;
    if (false && frames_counter > mSampleRate * 10.0)
    {
      frames_counter = 0;
      
      mParticle.set(0,0, 
      random(-1.5, 1.5), random(-1.5, 1.5)
      );
    }
    
    channels[0] = (float)mParticle.x + random(-1.0,1.0)*0.0125;
    channels[1] = (float)mParticle.y + random(-1.0,1.0)*0.025;
    
    mParticle.update(1.0/mSampleRate);
    */
  }
  
  double mSampleRate = 48000.0;
  int frames_counter;
  
  Particle mParticle = new Particle();
  
}