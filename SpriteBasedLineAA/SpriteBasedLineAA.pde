import processing.opengl.*;

PImage tex_smooth;

void setup() 
{
  size(800, 600, P3D);
  frameRate(60);
  smooth();
  //line(0,0,cx,cy);
  tex_smooth = loadImage("smooth_15x15.png");
}



void draw()
{
  float time = millis() / 1000.0;
  float tt = time * 0.5 * PI;
  
  background(20, 30, 160);
  
  float cx = width * 0.5;
  float cy = height * 0.5;  
  float r = width*0.2;
  int res = 100;
  
  tint(246, 240, 255);
  noStroke();
  //strokeWeight(3);  
 
  float lw = sin(tt)*10.0+11;
  drawCircleFakeAA(cx, cy, r, lw, 80, 0);
  
  float xx = cx+cos(tt*0.125)*r;
  float yy = cy+sin(tt*0.125)*r;  
  drawLineFakeAA(cx, cy, mouseX, mouseY, lw, true);
  
  /*float lxo=0, lyo=0;
  for(int i=0; i<4; i++)
  {
    float a = tt*0.33 + (i/3.0f)*TWO_PI;
    float lx = cos(a) * 100 + mouseX;
    float ly = sin(a) * 100 + mouseY;    
    if(i>0) drawLineFakeAA(lx, ly, lxo, lyo, lw, true);
    lxo = lx;
    lyo = ly;        
  }*/
  
  drawEquilateralTrinagleFakeAA(mouseX, mouseY, r*2, lw);
}

  void drawLineFakeAA(float xa, float ya, float xb, float yb, float w, boolean cap_ends)  
  {
    float dx = xb - xa;
    float dy = yb - ya;
    float len = sqrt(dx*dx + dy*dy);
    float a = atan2(dy, dx);
    float w2 = w*0.5;

    pushMatrix();
    translate(xa, ya);
    rotate(a);
    beginShape(TRIANGLE_STRIP);
    texture(tex_smooth);
    
    if(cap_ends)
    {
      // a
      vertex(-w2,-w2, 0, 0, 0);
      vertex(-w2,w2, 0, 0, 15);
      // b
      vertex(0,-w2, 0, 8, 0);
      vertex(0,w2, 0, 8, 15);        
    }

      // a
      vertex(0,-w2, 0, 7, 0);
      vertex(0,w2, 0, 7, 15);
      // b
      vertex(len,-w2, 0, 7, 0);
      vertex(len,w2, 0, 7, 15);      
      
    if(cap_ends)
    {
      // a
      vertex(len,-w2, 0, 7, 0);
      vertex(len,w2, 0, 7, 15);
      // b
      vertex(len+w2,-w2, 0, 0, 0);
      vertex(len+w2,w2, 0, 0, 15);        
    }      

    endShape();
    popMatrix();    
  }
  
  void drawEquilateralTrinagleFakeAA(float top_x, float top_y, float h_tri, float lw)
  {
    float r = h_tri  * 2.0/3.0;
    float cy = top_y + r;

    drawCircleFakeAA(top_x, cy, r, lw*2, 3, -PI*0.5);
  }

  void drawCircleFakeAA(float posx, float posy, float r, float lw, int resolution, float a0)
  {
    float incr = TWO_PI / resolution;
    float x, y;//, x2, y2;
    float a = a0;
    
   float min_lw = 5.0;
   float r_out, r_in;
   float v_out, v_in;
   
   //if(lw >= min_lw)
   {
    float lw2 = lw*0.5; 
    r_out = r + lw2;
    r_in = r - lw2;    
    v_out = 0;
    v_in = 15;
   }
   /*else
   {
    float lw2 = min_lw*0.5; 
    r_out = r + lw2;
    r_in = r - lw2;    
    float ht = 16;//0.5 * (min_lw / lw);
    v_out = 0-ht;
    v_in = 15+ht;     
   }*/
    

    pushMatrix();
    translate(posx, posy);
    beginShape(TRIANGLE_STRIP);
    texture(tex_smooth);
    for (int i = 0; i <= resolution; i++, a += incr)
    {
      x = cos(a);
      y = sin(a);
      vertex(x*r_out, y*r_out, 0, 7, v_out);
      vertex(x*r_in, y*r_in, 0, 7, v_in);      
    }
    endShape();
    popMatrix();
  }