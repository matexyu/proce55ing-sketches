
public static class ProcessingUtils {
  //
  public static void drawCircularSector(PApplet p, float innerRad, float outerRad, float angleinicial,
      float anglefinal, int pts)
  {
    float a = angleinicial;
    float da = (anglefinal - angleinicial) / pts;

    p.beginShape(PApplet.TRIANGLE_STRIP);
    
    for (int i = 0; i <= pts; i++)
    {      
      float sn = PApplet.sin(PApplet.radians(a));
      float cs = PApplet.cos(PApplet.radians(a));
      
      float px = cs * outerRad;
      float py = sn * outerRad;
      p.vertex(px, py);
      
      px = cs * innerRad;
      py = sn * innerRad;      
      p.vertex(px, py);
      
      a += da;
    }
    p.endShape();
    
  }

  public static void drawCircleWithLines(PApplet p, float posx, float posy, float r, int resolution)
  {
    float incr = PApplet.TWO_PI / resolution;
    float x, y;//, x2, y2;
    float a = 0;

    p.beginShape(PConstants.LINE_LOOP);
    for (int i = 0; i < resolution; i++, a += incr)
    {
      x = posx + r * PApplet.cos(a);
      y = posy + r * PApplet.sin(a);

      p.vertex(x, y);
    }
    p.endShape();
  }
  
  public static void drawCircleSectorWithLines(PApplet p, float posx, float posy, float radius, float radians_start, float radians_end, int smallest_segment_pixels)
  {    
    float delta_radians = radians_end - radians_start;
    int subdivisions = (int)(delta_radians * radius / (float)smallest_segment_pixels + 1.5f);
    float incr = delta_radians / (float)subdivisions;
    float x, y;//, x2, y2;
    //float a = (float)Math.PI + radians_start;
    float a = radians_start;

    p.beginShape(PConstants.LINE_STRIP);
    for (int i = 0; i <= subdivisions; i++, a += incr)
    {
      x = posx + radius * PApplet.cos(a);
      y = posy + radius * PApplet.sin(a);

      p.vertex(x, y);
    }
    p.endShape();
  }  

  // TODO: Optimize this
  public static void drawTextOnCircle(PApplet p, String message, float r, float angleini, float anglefi, int grey_level)
  {
      p.pushStyle();
    float theta;
    float deltaradians = PApplet.radians(anglefi - angleini);
    // add empty chars at beginning and end
    // TYODO: fix this mathematically
    message = "   " + message + "   ";
    float circleSegmentSize = r * deltaradians - 3 * p.textWidth(" ");

    int num_fitting_chars = 0;

    float last_letter_alpha = 1.0f;

    // find where we'll have to truncate the string
    float word_width = 0;
    for (int i = 0; i < message.length(); i++)
    {
      // for every letter
      char currentChar = message.charAt(i);
      // the character and its width
      float w = p.textWidth(currentChar);
      word_width += w;

      if (word_width > circleSegmentSize)
      {
        last_letter_alpha = 1.0f - (word_width - circleSegmentSize) / w;
        break;
      }
      num_fitting_chars++;
    }

    // Draw the curved text:
    float arclength;

    p.fill(grey_level);

    if (word_width > circleSegmentSize)
    {
      char currentChar = message.charAt(0);
      float w = p.textWidth(currentChar);
      arclength = -w;

      for (int i = 0; i < num_fitting_chars - 1; i++)
      {
        // implement nice fade in for the incoming latest letter
        boolean is_last_letter = (i == (num_fitting_chars - 2));
        if (is_last_letter) p.fill(grey_level, last_letter_alpha * 255.0f);

        // for every letter
        currentChar = message.charAt(i); // the character and its width

        w = p.textWidth(currentChar);
        arclength -= w / 2; // each box is centered so we move half the
        // width

        // angle in radians is the arclength divided by the radius
        theta = PApplet.radians(angleini) + deltaradians + arclength / r;

        p.pushMatrix();

        // polar to cartesian conversion allows us to find the point along
        // the curve
        p.translate(r * PApplet.cos(theta), r * PApplet.sin(theta));

        // rotate the box (rotation is offset by 90 degrees)
        p.rotate(theta - PConstants.PI / 2);

        p.textAlign(PApplet.CENTER);

        // Draw the character
        p.text(currentChar, 0, 0);

        p.popMatrix();

        // Move halfway again
        arclength -= w / 2;
      }
    }
    else
    // text fits completely into sector
    {
      arclength = -word_width / 2;
      for (int i = num_fitting_chars - 1; i >= 0; i--)
      { // for every letter
        char currentChar = message.charAt(i); // the character and its width
        float w = p.textWidth(currentChar);
        arclength += w / 2; // each box is centered so we move half the
        // width

        // angle in radians is the arclength divided by the radius
        theta = PApplet.radians(angleini) + deltaradians * 0.5f + arclength / r;

        p.pushMatrix();
        // polar to cartesian conversion allows us to find the point along
        // the curve
        p.translate(r * PApplet.cos(theta), r * PApplet.sin(theta));
        // rotate the box (rotation is offset by 90 degrees)
        p.rotate(theta - PConstants.PI / 2);

        p.textAlign(PApplet.CENTER);

        p.text(currentChar, 0, 0); // display the character
        p.popMatrix();

        // Move halfway again
        arclength += w / 2;
      }
    }
    
    p.popStyle();
  }

}
