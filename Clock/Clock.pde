
// https://processing.org/reference/libraries/pdf/
// http://toxi.co.uk/blog/2007/07/specifying-pdf-page-size-in-processing.htm

import processing.pdf.*;  
import ddf.minim.*;

import java.util.Calendar;

Minim minim;
AudioPlayer secondsTickSound;

final boolean gGeneratePdf = false;

boolean isDrawTimeEnabled = true;
boolean isSilentClock = false;

PFont f;
float ratio = 1.0;

float millimetersToPDFLibraryUnits(float millimeters)
{
  float pdf_units_per_mm = 180.0 / 510.0;
  return millimeters * pdf_units_per_mm;
}

void setup()
{
  size(1025, 1025);
  //size(510, 510, PDF, "Clock.pdf");
  
  // we pass this to Minim so that it can load files from the data directory
  minim = new Minim(this);
  
  // Load a soundfile from the /data folder of the sketch and play it back
  secondsTickSound = minim.loadFile("tick.wav");
  
  ratio = width / 1025.0;
  
  f = createFont("Arial", 120 * ratio);
  textFont(f);
  textAlign(CENTER, CENTER);
  smooth();
  frameRate(60);
}

void drawClockLine(float angle, float line_length, float line_thickness, float center_radius, boolean is_shadow)
{
  float d = center_radius * 2.0;
  boolean has_center = (center_radius > 0.0);
  
  pushStyle();
  
  if(is_shadow == false)
  {
    // Arm
    pushMatrix();
    rotate(angle + PI);
    strokeWeight(line_thickness);
    line(0.0, 0.0, 0.0, line_length);
    if(has_center)
    {
      noStroke();
      fill(g.strokeColor);
    }
    ellipse(0.0, 0.0, d, d);
    popMatrix();
  }
  else
  {
    // Shadow
    pushMatrix();
    translate(5, 10);
    rotate(angle + PI);
    noFill();
    stroke(0,32);
    strokeWeight(line_thickness);
    line(0.0, 0.0, 0.0, line_length);
    if(has_center)
    noStroke();
    fill(0,32);
    ellipse(0.0, 0.0, d, d);
    popMatrix();
  }
  popStyle();
}

int previousSecondsValue = -1;

void drawTime(float max_radius)
{
  Calendar now = Calendar.getInstance();
  int year = now.get(Calendar.YEAR);
  int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
  int day = now.get(Calendar.DAY_OF_MONTH);
  int hour = now.get(Calendar.HOUR_OF_DAY);
  int minute = now.get(Calendar.MINUTE);
  int second = now.get(Calendar.SECOND);
  int millis = now.get(Calendar.MILLISECOND);
  
  float seconds_phase = millis / 1000.0;
  float second_f = second + millis / 1000.0;
  float minute_f = minute + second_f / 60.0;
  float hour_f = hour + minute_f / 60.0;
  
  if(second != previousSecondsValue && isSilentClock == false)
  {
    secondsTickSound.rewind();
    secondsTickSound.setVolume(0.25 + random(0.2));
    secondsTickSound.play();
    previousSecondsValue = second;
  }
  
  noFill();
  
  float oscillation = sin(seconds_phase * 8.0 * PI * 2.0 + seconds_phase);
  float damping = 1.0 / (pow(seconds_phase, 2.0) * 200.0 + 1.0);
  damping = constrain(damping*1.1-0.1, 0.0, 1.0);
  float flick = 0.4 * oscillation * damping;
    
    float k = 0.1;
    damped_seconds = damped_seconds * k + (second + flick) * (1.0 - k);
  
  for(int i = 0; i < 2; i++)
  {
    boolean is_shadow = i == 0;
  // Hours
  stroke(16);
  drawClockLine(
    map(hour_f, 0.0, 24.0, 0.0, PI * 4.0),
    max_radius * 0.6, 
    15.0,
    0.0, is_shadow);
    
  // Minutes
  stroke(20);
  drawClockLine(
    map(minute_f, 0.0, 60.0, 0.0, PI * 2.0),
    max_radius * 0.9, 
    9.0,
    0.0, is_shadow);
    
  // Seconds
  stroke(240, 0, 0);
  if(isSilentClock)
  {
    drawClockLine(
      map(second_f, 0.0, 60.0, 0.0, PI * 2.0),
      max_radius, 
      5.0,
      15.0, is_shadow);
  }
  else
  {
    drawClockLine(
      map(damped_seconds, 0.0, 60.0, 0.0, PI * 2.0),
      max_radius, 
      5.0,
      15.0, is_shadow);
  }
  }
}

float damped_seconds = 0.0;

void draw() 
{
  background(255);
  float cx = width/2;
  float cy = height/2;
  float r = width/2;
  float d = r * 2;
  
  stroke(0);
  strokeWeight(1);
  noFill();
  ellipse(cx,cy, d, d);
  ellipse(cx,cy, 24, 24);
  
  int max_k = (isDrawTimeEnabled ? 1 : 200);
  for(int k = 0; k <= max_k; k++)
  {
  pushMatrix();
  float z = 1.0 / map(k, 0.0, max_k, 3.0, 1.0);
  float vv = map(k, 0.0, max_k, 1.0, 0.0);
  int val = (k == max_k) ? 0 : (int)((pow(vv, 0.25)*0.25+0.75) * 255.0);
  pushMatrix();
  translate(cx, cy);
  scale(z);
  rotate(vv*vv*vv*0.2 * (k % 2 == 0 ? -1.0 : 1.0));
  
  float a = -(float)Math.PI * 0.5;
  float da = (float)Math.PI * 2.0 / 60.0;
  
  for(int i = 0; i < 60; i++, a += da)
  {
    stroke(val);
    noFill();
    float nx = cos(a);
    float ny = sin(a);
    boolean is_hour_tick = (i % 5 == 0);
    float tick_size_ratio = is_hour_tick ? 0.13 : 0.1;
    float r_in = r * (1.0 - tick_size_ratio);
    float r_out = r;

    if(is_hour_tick)
    {
      strokeWeight(9 * ratio);
    }
    else
    {
      strokeWeight(3 * ratio);
    }
    
    line(nx * r_in, ny * r_in, nx * r_out, ny * r_out);
    
    if(k == max_k && is_hour_tick)
    {
       float r_text = r * 0.7;
       float tx = nx * r_text;
       float ty = ny * r_text;
       noStroke();
       fill(val);
       int hour = (((i / 5) - 1 + 12) % 12) + 1;
       text("" + hour, tx, ty);
    }
  }
  popMatrix();
  popMatrix();
  }
  
  if(isDrawTimeEnabled)
  {
    pushMatrix();
    translate(cx, cy);
    drawTime(r * 0.9);
    popMatrix();
  }

if(gGeneratePdf)
{
  // Exit the program
  println("Finished.");
  exit();
}
}