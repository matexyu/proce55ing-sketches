/**
 * Your Life in Weeks
 * as seen on Wait But Why - Your Life in Weeks
 * http://waitbutwhy.com/2014/05/life-weeks.html
 *
 * Processing 3 Sketch by Mathieu Bosi, november 2016
 */


import processing.pdf.*;

PFont header_font;
PFont year_row_font;

float header_font_size_px = 42; 
float year_row_font_size_px = 12;

void setup()
{
  size(1200, 2200);
  
  // Create the font
  //printArray(PFont.list());
  header_font = createFont("Arial", header_font_size_px);
  year_row_font = createFont("Arial", year_row_font_size_px);
}

void draw()
{
  boolean do_record_pdf_frame = (frameCount == 1);
  
  if(do_record_pdf_frame)
    beginRecord(PDF, "LifeCalendar.pdf"); 
  
  background(255);
  
  // Header
  {
    textFont(header_font);
    textAlign(CENTER);
    noStroke();
    fill(0);
    text("LIFE CALENDAR", width/2, header_font_size_px);
  }
  
  int max_years = 90;
  int weeks_per_year = 52;
  float header_height_px = header_font_size_px * 2.5;
  
  float min_y = header_height_px;
  float max_y = height - 10;
  float min_x = 20; 
  float max_x = width;
  
  float box_area_size = (max_x - min_x) / weeks_per_year;
  float box_edge_size_px = box_area_size - 4;
  
  for(int year = 0; year <= max_years; year++)
  {
    float year_y = map(year, 0, max_years, min_y, max_y);
    
    textFont(year_row_font);
    textAlign(RIGHT);
    noStroke();
    fill(0);
    text("" + (year), min_x - 4, year_y + year_row_font_size_px+2);
    
    noFill();
    stroke(0);
    strokeWeight(1);
    
    for(int week = 0; week < weeks_per_year; week++)
    {  
      float week_x = map(week, 0, weeks_per_year, min_x, max_x);
      
      if(year == 0)
      {
        textAlign(CENTER);
        noStroke();
        fill(0);
        text("" + (week + 1), 
             week_x + box_edge_size_px/2.0, 
             min_y - year_row_font_size_px+4);
        noFill();
        stroke(0);
      }
      
      rect(week_x, year_y, box_edge_size_px, box_edge_size_px); 
    }
  }
  
  if(do_record_pdf_frame)
  {
    endRecord();
    float h = min_y + box_area_size * max_years;
    println("min_y + box_area_size * max_years = " + h);
  }
}