
class Friend {
  Friend(float _x, float _y)
  {
    x = _x;
    y = _y;
    r = 15;
  }
  public float x, y, r;

  void drawInfluenceCircle(float radius, color col, float alpha)
  {
    alpha = constrain(alpha, 0.0, 1.0);
    noFill();
    stroke(color(col, (int)(alpha*255.0)));
    ellipse(x, y, radius, radius);
  }

  void draw(color col)
  {
    noStroke();
    fill(col);
    ellipse(x, y, r, r);
  }
}

ArrayList<Friend> mFriends;

void setup() 
{
  size(1280, 800);
  background(102);
  ellipseMode(RADIUS);

  mFriends = new ArrayList<Friend>();

  mFriends.add(mOutlet = new Friend(width / 2, height / 2));

  for (int i = 0; i < 16; i++)
  {
    float x = random(0, width);
    float y = random(0, height);
    mFriends.add(new Friend(x, y));
  }

  frameRate(60.0);
  smooth(2);

  mMaxConnectionDistance = width / 10.0;
}

Friend mMouse = new Friend(0.0, 0.0);
Friend mOutlet;

float mMinRadius;
float mMaxConnectionDistance;
float mPreviewEpsilon = 100;

void draw() 
{
  float current_min_radius = min(mMaxConnectionDistance * 0.5, mMinRadius) ;

  background(0);

  mMouse.x = mouseX;
  mMouse.y = mouseY;
  mMouse.r = 15;

  float min_distance = 0.0;
  float nearest_friend_alpha = 0.0;
  Friend nearest_friend = null;
  int i = 0;

  for (Friend friend : mFriends)
  { 
    friend.draw(i == 0 ? color(128, 128, 255) : color(255, 255, 0));

    float d = dist(friend.x, friend.y, mMouse.x, mMouse.y);

    if (d < mMaxConnectionDistance)
    { 
      friend.drawInfluenceCircle(current_min_radius, color(0, 255, 0), 1.0);

      if (nearest_friend == null)
      {
        nearest_friend = friend;
        min_distance = d;
      } else if (d < min_distance)
      {
        nearest_friend = friend;
        min_distance = d;
      }
    } 
    else if (d < mMaxConnectionDistance + mPreviewEpsilon)
    {
      float k = 1.0 - (d - mMaxConnectionDistance) / mPreviewEpsilon;
      friend.drawInfluenceCircle(current_min_radius, color(0, 255, 0), k);
      
      if(k > nearest_friend_alpha)
      {
        nearest_friend_alpha = k;
      }
    }

    i++;
  }

  if (nearest_friend != null)
  {
    noFill();
    stroke(255, 0, 0);
    strokeWeight(3.0);
    line(mMouse.x, mMouse.y, nearest_friend.x, nearest_friend.y);
    strokeWeight(1.0);
    mMinRadius = min_distance * 0.5;

    float mDisconnectPreviewDistance = mMaxConnectionDistance * 0.25;
    float lol = abs(min_distance - mMaxConnectionDistance);
    if (lol < mDisconnectPreviewDistance)
    {
      float k = lol / mDisconnectPreviewDistance;
      float dr = current_min_radius - mDisconnectPreviewDistance * k;
      mMouse.drawInfluenceCircle(dr, color(255, 255, 255), sqrt(1.0 - k));
    }
    
    nearest_friend_alpha = 1.0;
  }
  
  mMouse.draw(color(255, 0, 0));
  if(nearest_friend_alpha > 0.0)
  {
    mMouse.drawInfluenceCircle(current_min_radius, color(0, 255, 0), nearest_friend_alpha);
  }
}