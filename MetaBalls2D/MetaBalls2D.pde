
void setup()
{
  size(1024, 1024, P3D);
  blob = initBlob(512, 512);
  frameRate(60);
  noSmooth();
}

// formulas at: http://paulbourke.net/geometry/implicitsurf/

float getMetaballFieldAtRadius(float r, float a, float b)
{
  if(0.0 <= r && r <= b/3.0)
  {
    return a * (1.0 - (3.0 * r*r) / b*b);
  }
  if(b/3.0 < r && r <= b)
  {
    float k = (1.0 - r / b);
    return (3.0 * a / 2.0) * (k * k);
  }
  return 0.0f;
}

float getBlinnFieldAtRadius(float r, float a, float b)
{
  return a * exp(-b * r*r);
}

float getSoftObjectFieldAtRadius(float r, float a, float b)
{
  if(r > b) return 0.0;
  
  return a * (1.0 
  - 4.0  * pow(r, 6) / (9.0 * pow(b, 6))
  + 17.0 * pow(r, 4) / (9.0 * pow(b, 4))
  - 22.0 * pow(r, 2) / (9.0 * pow(b, 2)));
}

float getFieldAtRadius(float r)
{
  //return getSoftObjectFieldAtRadius(r, 1.0, 1.0);
  //return getMetaballFieldAtRadius(r, 1.0, 1.0);
  return getBlinnFieldAtRadius(r, 1.0, 5.0);
}

PImage blob;

PImage initBlob(int w, int h)
{
  PImage target = createImage(w, h, ARGB);
  target.loadPixels();
  
  int o = 0;
  for(int j = 0; j < h; j++)
  {
    float y = map(j, 0, h, 1.0, -1.0);
    float y2 = y*y;
    
    for(int i = 0; i < w; i++, o++)
    {
      float x = map(i, 0, w, -1.0, 1.0);
      float r = sqrt(x*x + y2);
      float f = getFieldAtRadius(r);
      target.pixels[o] = color(255, 255, 255, (int)(f*255.0));
    }
  }
  
  target.updatePixels();
  
  return target;
}

void drawBlob(float x, float y)
{
  image(blob, 
        x - blob.width / 2, 
        y - blob.height / 2);
}

void draw()
{  
  background(0);
  
  blendMode(ADD);
  
  drawBlob(mouseX, mouseY);
  drawBlob(width - mouseX, height - mouseY);
}