/*
 Live Video Light Painting
 Mathieu Bosi 2015 / 2016
 
 Partly based on: https://github.com/positlabs/lpl-processor
 http://lightpaintlive.com/

 KEYBOARD CONTROLS:
 - Spacebar: reset
 - Keys:
   . 'e'     : toggle exponent (emphasize bright areas)
   . 'd'     : toggle decay (forget lightness)
   . 'Enter' : save image to disk (timestamp-based filename)
 
 TO RUN IN FULLSCREEN:
 -  Shift + Command + R 

*/

import processing.video.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

boolean doEnableVlcTranscoding = false; // For GoPro over WiFi, VLC used as a transcoder. Does not work?

PFont font;

Capture video;
int captureWidth = 640;
int captureHeight = 480;

PGraphics canvas;
PGraphics canvas_exp;
int frameNum = 0;

boolean exp_image = false;
boolean image_decay_enabled = false;

class Camera {
  public Camera(int index, String p5_capture_config_string)
  {
    this.index = index;
    configString = p5_capture_config_string;
    // 'name=USB2.0 HD UVC WebCam,size=640x480,fps=30'
    String [] tokens = configString.split(",");
    
    name = tokens[0].split("=")[1];
    
    String [] size_token = tokens[1].split("=")[1].split("x");
    width = Integer.parseInt(size_token[0]);
    height = Integer.parseInt(size_token[1]);
    
    frameRate = Integer.parseInt(tokens[2].split("=")[1]);
  }
  
  String toString()
  {
    return 
    index + ": " + 
    name + ", " + 
    width + " x " + height + ", " +
    frameRate + " fps";
  }
  
  int index;
  public String configString;
  public String name;
  public int width;
  public int height;
  public int frameRate;
}

HashMap<String, ArrayList<Camera>> cameras = new HashMap<String, ArrayList<Camera>>();

ArrayList<String> cameraNames = new ArrayList<String>();

void setup() 
{
  size(displayWidth, displayHeight, P3D);
  
  dh = new DisposeHandler(this);
  
  frameRate(60);
  
  font = createFont("ArialMT-32", 32);
  textFont(font);

  String[] p5_cameras = Capture.list();
  
  if (p5_cameras.length == 0) 
  {
    println("There are no cameras available for capture.");
    exit();
  } 
  else 
  {
    for (int i = 0; i < p5_cameras.length; i++) 
    {
      Camera cam = new Camera(i, p5_cameras[i]);
      if(cameras.containsKey(cam.name) == false)
      {
        cameraNames.add(cam.name);
        cameras.put(cam.name, new ArrayList<Camera>());
      }
      ArrayList<Camera> cams = cameras.get(cam.name);
      cams.add(cam);
    }
  }
  
  println("Available cameras:");
  for(String camera_name : cameraNames)
  {
    println("-------------------------");
    println("Camera: '" + camera_name + "'");
    println("-------------------------");
  for(Camera cam : cameras.get(camera_name))
  {
    println(" " + cam.toString());
  }
  }
  
  // https://processing.org/reference/libraries/video/Capture.html
  Camera cam = cameras.get(cameraNames.get(0)).get(1);
  video = new Capture(this, cam.configString);//, captureWidth, captureHeight);
  video.width = cam.width;
  video.height = cam.height;
  background(0);
  
  // Prepare canavas
  canvas = createGraphics(video.width, video.height);
  canvas_exp = createGraphics(video.width, video.height);
  resetCanvas(canvas);
  resetCanvas(canvas_exp);
  
  if (doEnableVlcTranscoding)
  {
    launchVideoLanGoProTranscoder();
  }
  
  // Start capturing the images from the camera
  video.start();
}

DisposeHandler dh;

public class DisposeHandler {
  DisposeHandler(PApplet pa)
  {
    pa.registerMethod("dispose", this);
  }
  public void dispose()
  {      
    println("Closing sketch");
    // Place here the code you want to execute on exit
    println("dispose() called.");
    if(mVlcProcess != null)
    {
      println("Destroying VLC process...");
      mVlcProcess.destroy();
    }
  }
}

Process mVlcProcess;

void launchVideoLanGoProTranscoder()
{
  // Source: https://forum.videolan.org/viewtopic.php?t=125540
  String vlc_folder = "C:\\Program Files (x86)\\VideoLAN\\VLC\\";
  String gopro_transcoder_params = "vcodec=h264,acodec=mpga,ab=128,channels=2,samplerate=44100";
  String target_mjpeg_address = "8080/go.mjpg";
  try {
    mVlcProcess = new ProcessBuilder(vlc_folder + 
      "vlc.exe",
      "--vvv http://10.5.5.9:8080/live/amba.m3u8" +
      " --sout \"#transcode{" + gopro_transcoder_params + "}:http{mux=ffmpeg{mux=flv}," +
      "dst=:" + target_mjpeg_address + "}\"")
       .inheritIO()
       .directory(new File(vlc_folder))
       .start();
  }
  catch(Exception e)
  {
    println(e.toString());
  }
}

void resetCanvas(PGraphics c)
{
  c.beginDraw();
  c.background(0);
  c.endDraw();
}

void draw() 
{
  background(0);
  
  if (video.available() == false)
  {
    text("Video unavailable", 32, 32);
  }
  else
  {
    // When using video to manipulate the screen, use video.available() and
    // video.read() inside the draw() method so that it's safe to draw to the screen
    video.read(); // Read the new frame from the camera
    video.loadPixels(); // Make its pixels[] array available
    renderFrame(video);
    float best_fit_scale_factor = min(width / (float)canvas.width, height / (float)canvas.height);
    
    pushMatrix();
    translate(-(canvas.width*best_fit_scale_factor - width) * 0.5,
               (canvas.height*best_fit_scale_factor - height) * 0.5);
    scale(-best_fit_scale_factor, best_fit_scale_factor);
    translate(-canvas.width, 0);
    
    if(exp_image)
    {
      image(canvas_exp, 0, 0);
    }
    else
    {
      image(canvas, 0, 0);
    }
    popMatrix();
  }
}

void renderFrame(Capture camera) 
{
  int [] frame_pixels = camera.pixels;

  for (int i = 0; i < frame_pixels.length; i++) 
  {  
    color canvas_pixel_color = canvas.pixels[i];
    color frame_pixel_color = frame_pixels[i];
    
    // Luminance (perceived): (0.299*R + 0.587*G + 0.114*B)
    int cr = (canvas_pixel_color >> 16) & 0xff;
    int cg = (canvas_pixel_color >> 8 ) & 0xff;
    int cb = canvas_pixel_color & 0xff;
    
    int fr = (frame_pixel_color >> 16) & 0xff;
    int fg = (frame_pixel_color >> 8 ) & 0xff;
    int fb = frame_pixel_color & 0xff;
    
    float canvas_pixel_luminance = cr * .299 + cg * .587 + cb * .144;
    float frame_pixel_luminance = fr * .299 + fg * .587 + fb * .144;
    
    if(frame_pixel_luminance > canvas_pixel_luminance)
    {
      canvas.pixels[i] = frame_pixel_color;
    }
    else if(image_decay_enabled)
    {
      cr = (cr * 1022) >> 10;
      cg = (cg * 1022) >> 10;
      cb = (cb * 1022) >> 10;
      canvas.pixels[i] = 0xff000000 | (cr << 16) | (cg << 8) | cb;
    }
    
    if(exp_image)
    {
      color cc = canvas.pixels[i];
      // Luminance (perceived): (0.299*R + 0.587*G + 0.114*B)
      int rr = (cc >> 16) & 0xff;
      int gg = (cc >> 8 ) & 0xff;
      int bb = cc & 0xff;
      rr = (rr * rr) >> 8;
      gg = (gg * gg) >> 8;
      bb = (bb * bb) >> 8;
      canvas_exp.pixels[i] = (0xff000000) | (rr << 16) | (gg << 8) | (bb);//
    }
  }
  
  canvas.updatePixels();
  if(exp_image)
  {
    canvas_exp.updatePixels();
  }
}



void keyPressed()
{
  if(keyCode == ENTER)
  {
    // Create an instance of SimpleDateFormat used for formatting 
    // the string representation of date (month/day/year)
    DateFormat df = new SimpleDateFormat("MM-dd-yyyy_HH-mm-ss");

    // Get the date today using Calendar object.
    Date today = Calendar.getInstance().getTime();        
    // Using DateFormat format method we can create a string 
    // representation of a date with the defined format.
    String image_name = df.format(today) + ".png";
    println("Saving image: '" + image_name + "'");
    save(image_name);
  }
  if(key == ' ')
  {
    resetCanvas(canvas);
    resetCanvas(canvas_exp);
  }
  if(key == 'E' || key == 'e')
  {
    exp_image = !exp_image;
    resetCanvas(canvas);
    resetCanvas(canvas_exp);
  }
  if(key == 'D' || key == 'd')
  {
    image_decay_enabled = !image_decay_enabled;
  }
}