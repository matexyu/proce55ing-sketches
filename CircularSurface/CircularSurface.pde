//
// CircularSurface
//
// Based on:
// https://www.youtube.com/watch?v=dOSGS9ruj4U&feature=youtu.be
//

PShape circularSurface;
PGraphics drawingSurface;
int mTextureWidth;
int mTextureHeight;
float mCircleRadiusPx;

float speedIncrement = 0.5;
float rotationSpeedRatio = speedIncrement * 2.0;
float currentRotationSpeedRatio = rotationSpeedRatio;

float mFrameRate = 120.0;

PImage blob;
int blobAlpha = 100;
int blobDiameter = 55;

boolean goFullscreen = true;

void setup()
{
  fullScreen(P3D);
  //size(1920, 1000, P3D);
  
  blob = initBlob(blobDiameter*4, blobDiameter);
  
  // createCircularSurface
  float hx = width * 0.5;
  float hy = height * 0.5;
  mCircleRadiusPx = sqrt(hx*hx + hy*hy) + 1.0;
  float circumference_px = 2.0 * PI * mCircleRadiusPx;
  
  mTextureWidth = 4096;//(int)circumference_px;
  mTextureHeight = 1024;//(int)max_radius_px;
  drawingSurface = createGraphics(mTextureWidth, mTextureHeight, P3D);
  
  float pixels_per_slice = 10.0;
  int num_slices = round(circumference_px / pixels_per_slice);
  circularSurface = createCircularSurface(mCircleRadiusPx, num_slices, mTextureWidth, mTextureHeight);
  
  textureWrap(REPEAT);
  textureMode(NORMAL);
  
  frameRate(mFrameRate);
  
  noSmooth();
  drawingSurface.noSmooth();
}

// formulas at: http://paulbourke.net/geometry/implicitsurf/

float getMetaballFieldAtRadius(float r, float a, float b)
{
  if(0.0 <= r && r <= b/3.0)
  {
    return a * (1.0 - (3.0 * r*r) / b*b);
  }
  if(b/3.0 < r && r <= b)
  {
    float k = (1.0 - r / b);
    return (3.0 * a / 2.0) * (k * k);
  }
  return 0.0f;
}

float getBlinnFieldAtRadius(float r, float a, float b)
{
  return a * exp(-b * r*r);
}

float getSoftObjectFieldAtRadius(float r, float a, float b)
{
  if(r > b) return 0.0;
  
  return a * (1.0 
  - 4.0  * pow(r, 6) / (9.0 * pow(b, 6))
  + 17.0 * pow(r, 4) / (9.0 * pow(b, 4))
  - 22.0 * pow(r, 2) / (9.0 * pow(b, 2)));
}

float getFieldAtRadius(float r)
{
  //return getSoftObjectFieldAtRadius(r, 1.0, 1.0);
  //return getMetaballFieldAtRadius(r, 1.0, 1.0);
  return getBlinnFieldAtRadius(r, 1.0, 5.0);
}

PImage initBlob(int w, int h)
{
  PImage target = createImage(w, h, ARGB);
  target.loadPixels();
  
  int o = 0;
  for(int j = 0; j < h; j++)
  {
    float y = map(j, 0, h, 1.0, -1.0);
    float y2 = y*y;
    
    for(int i = 0; i < w; i++, o++)
    {
      float x = map(i, 0, w, -1.0, 1.0);
      float r = sqrt(x*x + y2);
      float f = getFieldAtRadius(r);
      target.pixels[o] = color(255, 255, 255, (int)(f*255.0));
    }
  }
  
  target.updatePixels();
  
  return target;
}

void drawBlob(PGraphics g, float x, float y)
{
  g.image(blob, 
          x - blob.width / 2, 
          y - blob.height / 2);
}

void drawOnSurface(color draw_color, float screen_x, float screen_y, float current_surface_rotation_angle, float radial_deviation)
{
  PGraphics g = drawingSurface;
  float x = screen_x - width * 0.5;
  float y = -(screen_y - height * 0.5);
  float r = sqrt(x*x + y*y) + radial_deviation;
  
  float angle = (current_surface_rotation_angle + atan2(y, x) + PI * 2.0);
  angle = angle % (PI * 2.0);
  float center_u = map(angle, PI*2.0, 0.0, 0, drawingSurface.width);
  
  float center_v = map(r, 0, mCircleRadiusPx, 0, drawingSurface.height);
  
  float ry = 32.0;
  float kr = map(r, 0, mCircleRadiusPx, 1.0, 0.0);
  float rx = ry * 0.5 * (kr*kr + 1.0);
  
  g.beginDraw();
  g.noStroke();
  g.tint(draw_color, blobAlpha);
  
  boolean draw_ellipse = false;
  
  if(draw_ellipse)
  {
    g.ellipse(center_u, center_v, rx, ry);
    if(center_u < rx)
    {
      g.ellipse(center_u + drawingSurface.width, center_v, rx, ry);
    }
    else if(center_u > drawingSurface.width - rx)
    {
      g.ellipse(center_u - drawingSurface.width, center_v, rx, ry);
    }
  }
  else
  {
    drawBlob(g, center_u, center_v);
    
    if(center_u < rx)
    {
      drawBlob(g, center_u + drawingSurface.width, center_v);
    }
    else if(center_u > drawingSurface.width - rx)
    {
      drawBlob(g, center_u - drawingSurface.width, center_v);
    }
  }
  
  g.endDraw();
}

void processKeys(int key, int keyCode)
{
  if (key == CODED)
  {
    if(keyCode == LEFT)  rotationSpeedRatio -= speedIncrement;
    if(keyCode == RIGHT) rotationSpeedRatio += speedIncrement;
    if(keyCode == DOWN)  rotationSpeedRatio = speedIncrement;
  }
}

void keyPressed()
{
    if (key != CODED)
  {
    // Spacebar
    if(keyCode == 32)
    {
      drawingSurface.beginDraw();
      drawingSurface.background(0);//165, 93, 53);
      drawingSurface.endDraw();
    }
  }
}

float currentAngle = 0.0;

void draw() 
{
  if(keyPressed)
  {
    processKeys(key, keyCode);
  }
  
  final float time_seconds = millis() / 1000.0;
  
  background(0);
  
  float k = 0.02;
  float speed_ratio = currentRotationSpeedRatio * (1.0 - k) + rotationSpeedRatio * k;
  
  float dt = 0.5 / mFrameRate;
  float da = speed_ratio * (PI * 2.0) * dt;
  currentAngle += da;
  
  float wiggle_omega = sin(time_seconds*PI*2.0*4.0)*0.2;
  float wiggle_rho = cos(time_seconds*PI*2.0*4.0);
  
  if(mousePressed)
  {
    int hue_value = (int)(2.0 * time_seconds) % 255;
    
    drawOnSurface(HsvToRgb(hue_value, 250, 128), mouseX, mouseY, currentAngle, wiggle_rho * 32.0);
  }
  
  // image(drawingSurface, 0, 0)
  noStroke();
  tint(255, 255);
  pushMatrix();
  translate(width/2.0, height/2.0);
  rotateZ(currentAngle);
  circularSurface.draw(getGraphics());
  stroke(255, 64);
  noFill();
  line(0,0,mCircleRadiusPx,0);
  popMatrix();
  
  fill(128, 200, 255);
  g.ellipse(width/2.0, height/2.0, 5, 5);
}

class HsvColor {
  HsvColor(int h, int s, int v)
  {
    this.h = h;
    this.s = s;
    this.v = v;
  }
  public int h;
  public int s;
  public int v;
}

class RgbColor 
{
  public int r;
  public int g;
  public int b;
  
  color toColor()
  {
    return color(r, g, b);
  }
}

color HsvToRgb(int h, int s, int v)
{
    HsvColor hsv = new HsvColor(h, s, v);
    RgbColor rgb = new RgbColor();
    int region, remainder, p, q, t;

    if (hsv.s == 0)
    {
        rgb.r = hsv.v;
        rgb.g = hsv.v;
        rgb.b = hsv.v;
        return rgb.toColor();
    }

    region = hsv.h / 43;
    remainder = (hsv.h - (region * 43)) * 6; 

    p = (hsv.v * (255 - hsv.s)) >> 8;
    q = (hsv.v * (255 - ((hsv.s * remainder) >> 8))) >> 8;
    t = (hsv.v * (255 - ((hsv.s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
        case 0:
            rgb.r = hsv.v; rgb.g = t; rgb.b = p;
            break;
        case 1:
            rgb.r = q; rgb.g = hsv.v; rgb.b = p;
            break;
        case 2:
            rgb.r = p; rgb.g = hsv.v; rgb.b = t;
            break;
        case 3:
            rgb.r = p; rgb.g = q; rgb.b = hsv.v;
            break;
        case 4:
            rgb.r = t; rgb.g = p; rgb.b = hsv.v;
            break;
        default:
            rgb.r = hsv.v; rgb.g = p; rgb.b = q;
            break;
    }

    return rgb.toColor();
}

PShape createCircularSurface(float radius, int num_slices, int texture_width, int texture_height)
{
  int th = texture_height - 1;
  
  //  QUAD_STRIP:
  //  0   2   4
  //  | / | / |
  //  1   3   ...
  PShape circle = createShape();
  circle.setTexture(drawingSurface);
  circle.beginShape(QUAD_STRIP);
  circle.noStroke();
  circle.tint(255);
  //circle.set3D(false);
  
  for(int i = 0; i < num_slices + 1; i++)
  {
    float a = map(i, 0, num_slices, 0.0, PI * 2.0);
    float u = map(i, 0, num_slices, 0.0, texture_width);
    float x = cos(a) * radius;
    float y = sin(a) * radius;
    
    circle.vertex(0.0, 0.0, u, 0.0);
    circle.vertex(x,   y,   u, th );
  }
  
  circle.endShape();
  
  return circle;
}

void drawCube(PGraphics cube, float xd, float yd)
{
  cube.beginDraw();
  cube.lights();
  cube.clear();
  cube.noStroke();
  cube.translate(cube.width/2, cube.height/2);
  cube.rotateX(frameCount/xd);
  cube.rotateY(frameCount/yd);
  cube.box(80);
  cube.endDraw();
}