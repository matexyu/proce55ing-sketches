/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mathieu Bosi <mathieu.bosi@gmail.com>
 */
public class Circle {

    public Circle()
    {
    }

    public Circle(Point2 center, double radius)
    {
        set(center, radius);
    }

    public void set(Point2 center, double radius)
    {
        mCenter = center;
        mRadius = radius;
    }
    Point2 mCenter;
    double mRadius;
}
