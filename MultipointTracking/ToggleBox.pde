
public class ToggleBox {

  public ToggleBox()
  {
  }
  
  public ToggleBox(float x, float y, float w, float h)
  {
    set(x, y, w, h);
  }
  
  public void set(float x, float y, float w, float h)
  {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  
  public void draw()
  {
    rectMode(CORNER);
    stroke(0);
    if(mIsActive) fill(255,200,200); 
     else fill(200,200,200);
     
    rect(x, y, w, h);
    

    {
       noStroke();
        fill(0);
        float tw = textWidth(mText);
       float th = textAscent() + textDescent();
       text(mText, x+(w-tw)*0.5, y+h*0.5-th);
    }
    if(mIsActive)
    {
      float tw = textWidth(mDescription);
      float th = textAscent() + textDescent();
      float tx = x + w / 2.0 - tw/2;
      float ty = y+h + 40 + th;
      
      float border_x = 24;
      float border_y = 16;
      float xl = x+w/2;

      float b = 4;
            
      stroke(128);
      line(xl, y+h, xl, ty-border_y);
      
      noStroke();
      fill(0,32);
      rect(tx-border_x+b, ty-border_y+b, tw+border_x*2, th+border_y*2);
      
      stroke(128);
      fill(255,255,200);
      rect(tx-border_x, ty-border_y, tw+border_x*2, th+border_y*2);
      
      noStroke();
      fill(0);
      text(mDescription, tx, ty+th);
    }
  }
  
  boolean isCircleContained(Circle c)
  {
    float edge_u = y;
    float edge_d = y+h;
    float edge_l = x;
    float edge_r = x+w;
    float cx = (float)c.mCenter.x;
    float cy = (float)c.mCenter.y;
    float r = (float)c.mRadius;
    return 
      (cx - r >= edge_l) && (cx + r <= edge_r) && 
      (cy - r >= edge_u) && (cy + r <= edge_d);
  }
  
  public boolean mIsActive;
  
  public float x, y, w, h;
  int mInstance;
  public String mText;
  public String mDescription;
}
