/**
 *
 * @author Mathieu Bosi <mathieu.bosi@gmail.com>
 */
public class Segment2D {

    public Point2 m;
    public Point2 a, b;
    private Segment2D axis;

    public Segment2D()
    {
        a = new Point2();
        b = new Point2();
        m = new Point2();
        axis = null;
    }

    public Segment2D(Point2 pa, Point2 pb)
    {
        a = new Point2();
        b = new Point2();
        m = new Point2();
        set(pa, pb);
    }

    private void updateMidPoint()
    {
        // Trigger axis recomputation
        axis = null;

        m.x = (a.x + b.x) * 0.5;
        m.y = (a.y + b.y) * 0.5;
    }

    void set(Point2 pa, Point2 pb)
    {
        a.x = pa.x;
        a.y = pa.y;
        b.x = pb.x;
        b.y = pb.y;
        updateMidPoint();
    }

    void set(double ax, double ay, double bx, double by)
    {
        a.x = ax;
        a.y = ay;
        b.x = bx;
        b.y = by;
        updateMidPoint();
    }

    public double getLength()
    {
        double dx = b.x - a.x;
        double dy = b.y - a.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public Double getAngle(Segment2D s)
    {
        double vx = b.x - a.x;
        double vy = b.y - a.y;
        double len = getLength();
        double svx = s.b.x - s.a.x;
        double svy = s.b.y - s.a.y;
        double slen = s.getLength();
        if (len == 0.0 || slen == 0.0)
        {
            return null;
        }
        vx /= len;
        vy /= len;
        svx /= slen;
        svy /= slen;
        double dot_product = vx * svx + vy * svy;
        return Math.acos(dot_product) * 180.0 / Math.PI;
    }

    public Segment2D getAxis()
    {
        if (axis != null)
        {
            return axis;
        }
        else
        {
            axis = new Segment2D();
        }
        double vx = b.y - a.y;
        double vy = -(b.x - a.x);

        axis.set(m, new Point2(m.x - vx, m.y - vy));

        return axis;
    }
}