
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Mathieu Bosi <mathieu.bosi@gmail.com>
 */
public class Utils {

    /*
     // http://rosettacode.org/wiki/Power_Set#Iterative
     public  <T> List<List<T>> powerSetN(Collection<T> list, int num)
     {
     List<List<T>> ps = new ArrayList<List<T>>();
     ps.add(new ArrayList<T>());   // add the empty set
     // for every item in the original list
     for (T item : list)
     {
     List<List<T>> newPs = new ArrayList<List<T>>();
     for (List<T> subset : ps)
     {
     // copy all of the current powerset's subsets
     newPs.add(subset);
     // plus the subsets appended with the current item
     List<T> newSubset = new ArrayList<T>(subset);
     newSubset.add(item);
     newPs.add(newSubset);
     }
     // powerset is now powerset of list.subList(0, list.indexOf(item)+1)
     ps = newPs;
     }
     //return ps;

     // Only return Power Sets with 'num' elements
     List<List<T>> ps_num = new ArrayList<List<T>>();
     for (List<T> l : ps)
     {
     if (l.size() == num)
     {
     ps_num.add(l);
     }
     }
     return ps_num;
     }
     */
    public  <T> Set<Set<T>> powerSetN(Set<T> originalSet, int num)
    {
        Set<Set<T>> sets = powerSet(originalSet);
        // Filter by power
        Set<Set<T>> sets_n = new HashSet<Set<T>>();
        for (Set<T> s : sets)
        {
            if (s.size() == num)
            {
                sets_n.add(s);
            }
        }
        return sets_n;
    }

    // http://stackoverflow.com/questions/1670862/obtaining-powerset-of-a-set-in-java
    public  <T> Set<Set<T>> powerSet(Set<T> originalSet)
    {
        Set<Set<T>> sets = new HashSet<Set<T>>();
        if (originalSet.isEmpty())
        {
            sets.add(new HashSet<T>());
            return sets;
        }
        List<T> list = new ArrayList<T>(originalSet);
        T head = list.get(0);
        Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest))
        {
            Set<T> newSet = new HashSet<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }

    // Returns a Point2 if the lines intersect, otherwise null
    public  Point2 getIntersection(
            double p0_x, double p0_y, double p1_x, double p1_y,
            double p2_x, double p2_y, double p3_x, double p3_y)
    {
        double s1_x, s1_y, s2_x, s2_y;
        s1_x = p1_x - p0_x;
        s1_y = p1_y - p0_y;
        s2_x = p3_x - p2_x;
        s2_y = p3_y - p2_y;

        double s, t;
        double delta = -s2_x * s1_y + s1_x * s2_y;

        if (delta != 0.0)
        {
            s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / delta;
            t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / delta;
            //if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            {
                double i_x = p0_x + (t * s1_x);
                double i_y = p0_y + (t * s1_y);

                return new Point2(i_x, i_y);
            }
        }
        else
        {
            //println("Segments are Parallel!");
        }

        return null; // No intersection
    }

    public  Circle getCircleFromSegments(Segment2D sa, Segment2D sb)
    {
        Segment2D a1 = sa.getAxis();
        Segment2D a2 = sb.getAxis();
        Point2 intr =
                mUtils.getIntersection(
                a1.a.x, a1.a.y, a1.b.x, a1.b.y,
                a2.a.x, a2.a.y, a2.b.x, a2.b.y);

        if (intr == null)
        {
            return null;
        }

        double dx = sa.a.x - intr.x;
        double dy = sa.a.y - intr.y;
        double radius = (double) Math.sqrt(dx * dx + dy * dy);

        return new Circle(intr, radius);
    }

    public  Point2 getIntersectionFromSegments(Segment2D a1, Segment2D a2)
    {
        Point2 intr =
                mUtils.getIntersection(
                a1.a.x, a1.a.y, a1.b.x, a1.b.y,
                a2.a.x, a2.a.y, a2.b.x, a2.b.y);

        if (intr == null)
        {
            return null;
        }

        return intr;
    }

     Circle[] findCirclesFromPointsByRadiusAndNumPoints(Point2[] points,
            double object_radius, double object_radius_margin, int object_num_points)
    {
        int max_objects = points.length / object_num_points;

        ArrayList<Circle> object_circles = new ArrayList<Circle>();
        //ArrayList<Point2> points_list = new ArrayList<Point2>(Arrays.asList(points));
        // Retrieve the specified Point2 Power Sets
        //List<List<Point2>> candidate_points_tuples = Utils.powerSetN(points_list, object_num_points);

        // Convert from array to Set
        Set<Point2> points_set = new HashSet<Point2>();
        for (Point2 p : points)
        {
            points_set.add(p);
        }

        Set<Set<Point2>> candidate_points_sets = powerSetN(points_set, object_num_points);
        /*println(points_list.size()
         + " points originated " + candidate_points_sets.size()
         + " sets with power " + object_num_points);
         */
        for (Set<Point2> current_points_set : candidate_points_sets)
        {
            ArrayList<Point2> current_points_tuple = new ArrayList<Point2>();
            for (Point2 p : current_points_set)
            {
                current_points_tuple.add(p);
            }
            // See if this tuple potentially lies on the object's circle
            boolean is_tuple_valid = true;
            for (int i = 0; i < current_points_tuple.size() - 1 && is_tuple_valid; i++)
            {
                Segment2D s = new Segment2D(current_points_tuple.get(i),
                        current_points_tuple.get(i + 1));
                double half_length = (double) (s.getLength() * 0.5);
                if (half_length > object_radius + object_radius_margin)
                {
                    is_tuple_valid = false;
                }
            }
            if (is_tuple_valid == false)
            {
                continue;
            }

            ArrayList<Segment2D> candidate_segments = new ArrayList<Segment2D>();
            for (int i = 0; i < current_points_tuple.size() - 1; i++)
            {
                Segment2D s = new Segment2D();
                s.set(current_points_tuple.get(i),
                        current_points_tuple.get(i + 1));
                candidate_segments.add(s);
            }

            /// ...really on the object circle?
            Circle circ = null;
            for (int i = 0; i < candidate_segments.size() - 1; i++)
            {
                Segment2D sa = candidate_segments.get(i);
                Segment2D sb = candidate_segments.get(i + 1);
                circ = getCircleFromSegments(sa, sb);
                // Degenerate case
                if (circ == null)
                {
                    println("Degenerate Circle!");
                    break;
                }
                // Check if the radius is within margin
                double dr = Math.abs(circ.mRadius - object_radius);
                if (dr > object_radius_margin)
                {
                    circ = null;
                    break;
                }
            }
            // The tuple survived, it's most certainly a circle belonging to an object
            if (circ != null)
            {
                // Last check: are sides compatible with an regular polygon?
                // http://en.wikipedia.org/wiki/Regular_polygon#Circumradius
                double radius = circ.mRadius;
                double side = radius * 2.0 * Math.sin(Math.PI / (double) object_num_points);
                boolean is_ok = true;
                for (Segment2D s : candidate_segments)
                {
                    double len = s.getLength();
                    if (Math.abs(len - side) > object_radius_margin)
                    {
                        is_ok = false;
                        break;
                    }
                }
                if (is_ok)
                {
                    object_circles.add(circ);
/*                    if (object_circles.size() >= max_objects)
                    {
                        break;
                    }*/
                }
            }
        }
        //return object_circles.toArray(
          //      new Circle[object_circles.size()]);
        

        ArrayList<Circle> object_circles_non_overlapping = new ArrayList<Circle>();
        for (Circle ca : object_circles)
        {
            boolean has_overlap = false;
            for (Circle cb : object_circles)
            {
                if (cb != ca)
                {
                    Segment2D d = new Segment2D(ca.mCenter, cb.mCenter);
                    if (d.getLength() * 0.5 < object_radius)
                    {
                        has_overlap = true;
                        break;
                    }
                }
            }
            if (!has_overlap && !object_circles_non_overlapping.contains(ca))
            {
                object_circles_non_overlapping.add(ca);
            }
        }

        return object_circles_non_overlapping.toArray(
                new Circle[object_circles_non_overlapping.size()]);
 

    }
}