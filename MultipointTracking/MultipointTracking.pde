/**
  Custom multitouch handling 
  Mathieu Bosi 2014
  
  Uses SMT library to receive "touch events" 
  NOTE: SMT wraps Touch2Tuio.exe: http://dm.tzi.de/touch2tuio/
  Maybe we could just directly receive TUIO and get rid of SMT
*/

import vialab.SMT.*;

boolean mUseSMT = false;

// Object detection constants
static final int kObjectsNumPoints = 3;
static final int kObjectsRadius = 90; // <<<------------- CAMBIA QUESTO
static final double kObjectsRadiusMargin = 5;

Point2[] mPoints;
final int kMaxTouches = 20;

Segment2D[] mSegments;
Segment2D[] mAxes;

Utils mUtils = new Utils();

ToggleBox mToggleBoxes[] = new ToggleBox[5];

PFont mFont;

public void setup()
{
    // Use a fullscreen viewport 
    size(displayWidth, displayHeight, P3D);
    
    // Initialize SMT library
    if(mUseSMT) SMT.init(this, TouchSource.MULTIPLE);
    
    //frameRate(60);
    
    ellipseMode(RADIUS);
    smooth();
    mFont = createFont("Arial", 24);
    textFont(mFont);
    
    int x_padding = 50;
    int spacing = 20;
    int rr = (width - x_padding*2) / 5 ;//(int)(kObjectsRadius*2.0 + 40);
    int ox = x_padding;
    int oy = (height - (rr-spacing))/2 - spacing;
    
    for (int i = 0; i < mToggleBoxes.length; i++)
    {
      mToggleBoxes[i] = new ToggleBox(ox, oy, rr-spacing, rr-spacing);
      mToggleBoxes[i].mText = "" + (i+1);
      mToggleBoxes[i].mDescription = "Box number " + (i+1);
      ox += rr;
    }

    mPoints = new Point2[kMaxTouches];
    for (int i = 0; i < mPoints.length; i++)
    {
        mPoints[i] = new Point2();
    }

    mSegments = new Segment2D[mPoints.length - 1];
    mAxes = new Segment2D[mPoints.length - 1];
    for (int i = 0; i < mPoints.length - 1; i++)
    {
        mSegments[i] = new Segment2D();
        mSegments[i].set(mPoints[i], mPoints[i + 1]);
    }
}

void circle(double x, double y, double r)
{
    ellipse((float) x, (float) y, (float) r, (float) r);
}

void updateDummyTouches(Point2[] touches)
{
    double r = kObjectsRadius;// + 25.0f * kr;

    float a0 = (float) (0.1 * 2.0 * PI * millis() / 1000.0);
    for (int i = 0; i < 3; i++)
    {
        float a = (float) (i * PI * 2.0 / 3.0);
        touches[i].x = (int)(cos(a) * r + 200.0);
        touches[i].y = (int)(sin(a) * r + 200.0);
    }

    for (int i = 3; i < 6; i++)
    {
        float a = a0 + PI + (float) (i * PI * 2.0 / 3.0);
        touches[i].x = (int)(cos(a) * r + mouseX);
        touches[i].y = (int)(sin(a) * r + mouseY);
    }

    for (int i = 6; i < 9; i++)
    {
        float a = a0 + PI + (float) (i * PI * 2.0 / 3.0);
        touches[i].x = (int)(mouseX - cos(a) * r + 400);
        touches[i].y = (int)(height - (mouseY - sin(a) * r));
    }

    //touches[5].set(mouseX, mouseY);
}

int mNumTouches = 0;

void updateAnimation()
{    
  
    // Get the currently active touches
    
    if(mUseSMT)
    {
      Touch[] touches = SMT.getTouches();
      int it = 0;
      mNumTouches = 0;
      for(Touch t : touches)
      {
        if(t.isDown && mNumTouches < kMaxTouches)
        {
            mPoints[it].set(t.x, t.y);
            it++;
            mNumTouches++;
        }
      }
    }
    else // No SMT: dummy touches
    {
      updateDummyTouches(mPoints);
      mNumTouches = 9;
    }
    
    for (int i = 0; i < mNumTouches - 1; i++)
    {
        mSegments[i].set(mPoints[i], mPoints[i + 1]);
        mAxes[i] = mSegments[i].getAxis();
    }
}

public void drawPoints(Point2[] points)
{
    for (Point2 p : points)
    {
        circle(p.x, p.y, 20.0);
    }
}

public void drawSegments(Segment2D[] segments)
{
    for (Segment2D s : segments)
    {
        line((float) s.a.x, (float) s.a.y, (float) s.b.x, (float) s.b.y);
    }
}

public void draw()
{
    updateAnimation();
    background(255);

   Circle[] object_circles = null;
   Point2[] points = null;

  if(mNumTouches > 0)
  {
    points = new Point2[mNumTouches];
    for(int i = 0; i < points.length; i++)
    {
      points[i] = mPoints[i];
    }
    object_circles = mUtils.findCirclesFromPointsByRadiusAndNumPoints(
            points, kObjectsRadius, kObjectsRadiusMargin, kObjectsNumPoints);
  }
  
  for(ToggleBox tb : mToggleBoxes)
  {
      int num_contained_circles = 0;
      if(object_circles != null)
      for (Circle circ : object_circles)
      {
          if(tb.isCircleContained(circ))
          { 
            num_contained_circles++;
          }
      }
      tb.mIsActive = (num_contained_circles > 0);
  }  
  
  for(ToggleBox tb : mToggleBoxes)
  {
    tb.draw();
  }
  
  if(object_circles != null)
    for (Circle circ : object_circles)
    {
        noStroke();
        fill(255, 0, 0);
        circle(circ.mCenter.x, circ.mCenter.y, 2.5f);

        noFill();
        stroke(0);
        circle(circ.mCenter.x, circ.mCenter.y, circ.mRadius);

        text("R = " + (int) circ.mRadius,
                (float) circ.mCenter.x + 8, (float) circ.mCenter.y);
                
    }
    
    if(points != null)
    {
      noStroke();
      fill(0);
      drawPoints(points);
    }
    
if(object_circles != null)
    if (object_circles.length == 0)
    {
        noFill();
        stroke(255, 255, 0);
        drawSegments(mSegments);

        noFill();
        stroke(0, 0, 255);
        drawSegments(mAxes);

        drawSegmentsCircles(mSegments);
    }
    
    
}

void drawSegmentsCircles(Segment2D[] segments)
{
    for (int i = 0; i < segments.length - 1; i++)
    {
        Circle circ = mUtils.getCircleFromSegments(segments[i], segments[i + 1]);
        if (circ != null)
        {
            noStroke();
            fill(255, 0, 0);
            circle(circ.mCenter.x, circ.mCenter.y, 2.5f);

            noFill();
            stroke(200);
            circle(circ.mCenter.x, circ.mCenter.y, circ.mRadius);

            text("R = " + (int) circ.mRadius,
                    (float) circ.mCenter.x + 8, (float) circ.mCenter.y);
        }
    }
}

