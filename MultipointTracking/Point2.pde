/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mathieu Bosi <mathieu.bosi@gmail.com>
 */
public class Point2 {

    public Point2()
    {
    }

    public Point2(double x, double y)
    {
        set(x, y);
    }

    public void set(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    
    double x, y;
}
